# French translations for the reiser4progs manpages.
# Copyright (C) 2002, 2009, 2014, Free Software Foundation, Inc.
# This file is distributed under the same license as the reiser4progs package.
#
# Guillaume Bour, 2002.
# Nicolas François <nicolas.francois@centraliens.net>, 2009.
# David Prévot <david@tilapin.org>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: manpages-fr-extra-reiser4progs\n"
"POT-Creation-Date: 2014-08-03 12:37-0400\n"
"PO-Revision-Date: 2014-08-03 13:02-0400\n"
"Last-Translator: David Prévot <david@tilapin.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"

#. type: TH
#: C/man8/debugfs.reiser4.8:5
#, no-wrap
msgid "debugfs.reiser4"
msgstr "debugfs.reiser4"

#. type: TH
#: C/man8/debugfs.reiser4.8:5 C/man8/mkfs.reiser4.8:5
#, no-wrap
msgid "02 Oct, 2002"
msgstr "2 octobre 2002"

#. type: TH
#: C/man8/debugfs.reiser4.8:5 C/man8/mkfs.reiser4.8:5 C/man8/fsck.reiser4.8:5
#: C/man8/measurefs.reiser4.8:5
#, no-wrap
msgid "reiser4progs"
msgstr "reiser4progs"

#. type: TH
#: C/man8/debugfs.reiser4.8:5 C/man8/mkfs.reiser4.8:5 C/man8/fsck.reiser4.8:5
#: C/man8/measurefs.reiser4.8:5
#, no-wrap
msgid "reiser4progs manual"
msgstr "manuel de reiser4progs"

#.  Please adjust this date whenever revising the manpage.
#.  Some roff macros, for reference:
#.  .nh        disable hyphenation
#.  .hy        enable hyphenation
#.  .ad l      left justify
#.  .ad b      justify to both left and right margins
#.  .nf        disable filling
#.  .fi        enable filling
#.  .br        insert line break
#.  .sp <n>    insert n+1 empty lines
#.  for manpage-specific macros, see man(7)
#. type: SH
#: C/man8/debugfs.reiser4.8:18 C/man8/mkfs.reiser4.8:18
#: C/man8/fsck.reiser4.8:18 C/man8/measurefs.reiser4.8:18
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:20
msgid "debugfs.reiser4 - the program for debugging reiser4 filesystem."
msgstr "debugfs.reiser4 - Déboguer un système de fichiers Reiser4"

#. type: SH
#: C/man8/debugfs.reiser4.8:20 C/man8/mkfs.reiser4.8:20
#: C/man8/fsck.reiser4.8:20 C/man8/measurefs.reiser4.8:21
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:23
msgid "B<debugfs.reiser4> [ options ] FILE"
msgstr "B<debugfs.reiser4> [I<options>] I<fichier>"

#. type: SH
#: C/man8/debugfs.reiser4.8:23 C/man8/mkfs.reiser4.8:23
#: C/man8/fsck.reiser4.8:23 C/man8/measurefs.reiser4.8:24
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

# NOTE: disscover
#. type: Plain text
#: C/man8/debugfs.reiser4.8:27
msgid ""
"B<debugfs.reiser4> is reiser4 filesystem debug program. You can disscover "
"the internal reiser4 filesystem structures by using it."
msgstr ""
"B<debugfs.reiser4> est le programme de débogage des systèmes de fichiers "
"Reiser4. Vous pouvez découvrir les structures internes d'un système de "
"fichiers Reiser4 en l'utilisant."

#. type: SH
#: C/man8/debugfs.reiser4.8:27 C/man8/mkfs.reiser4.8:29
#: C/man8/fsck.reiser4.8:69 C/man8/measurefs.reiser4.8:28
#, no-wrap
msgid "COMMON OPTIONS"
msgstr "OPTIONS COMMUNES"

#. type: TP
#: C/man8/debugfs.reiser4.8:28 C/man8/mkfs.reiser4.8:30
#: C/man8/fsck.reiser4.8:70 C/man8/measurefs.reiser4.8:29
#, no-wrap
msgid "B<-V, --version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:31 C/man8/mkfs.reiser4.8:33
#: C/man8/measurefs.reiser4.8:32
msgid "prints program version."
msgstr "Afficher la version du programme."

#. type: TP
#: C/man8/debugfs.reiser4.8:31 C/man8/mkfs.reiser4.8:33
#: C/man8/fsck.reiser4.8:73 C/man8/measurefs.reiser4.8:32
#, no-wrap
msgid "B<-?, -h, --help>"
msgstr "B<-?>, B<-h>, B<--help>"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:34 C/man8/mkfs.reiser4.8:36
#: C/man8/measurefs.reiser4.8:35
msgid "prints program help."
msgstr "Afficher le message d'aide du programme."

#. type: TP
#: C/man8/debugfs.reiser4.8:34 C/man8/mkfs.reiser4.8:36
#: C/man8/fsck.reiser4.8:79 C/man8/measurefs.reiser4.8:35
#, no-wrap
msgid "B<-y, --yes>"
msgstr "B<-y>, B<--yes>"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:37 C/man8/mkfs.reiser4.8:39
#: C/man8/fsck.reiser4.8:82 C/man8/measurefs.reiser4.8:38
msgid "assumes an answer 'yes' to all questions."
msgstr "Considérer que toutes les questions reçoivent une réponse affirmative."

#. type: TP
#: C/man8/debugfs.reiser4.8:37 C/man8/mkfs.reiser4.8:39
#: C/man8/fsck.reiser4.8:82 C/man8/measurefs.reiser4.8:38
#, no-wrap
msgid "B<-f, --force>"
msgstr "B<-f>, B<--force>"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:40
msgid ""
"forces debugfs to use whole disk, not block device or mounted partition."
msgstr ""
"Forcer B<debugfs> à utiliser un disque entier, au lieu d'un périphérique "
"bloc ou d'une partition montée."

#. type: TP
#: C/man8/debugfs.reiser4.8:40 C/man8/fsck.reiser4.8:88
#: C/man8/measurefs.reiser4.8:41
#, no-wrap
msgid "B<-c, --cache N>"
msgstr "B<-c>, B<--cache> I<n>"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:44 C/man8/measurefs.reiser4.8:45
msgid ""
"sets tree cache node number to passed value. This affects very much behavior "
"of libreiser4. It affects speed, tree allocation, etc."
msgstr ""
"Définir le I<n>ombre de nœud de l'arbre du cache. Cela affecte le "
"comportement de libreiser4 de façon importante : la vitesse, l'allocation "
"des arbres, etc."

#. type: SH
#: C/man8/debugfs.reiser4.8:44
#, no-wrap
msgid "BROWSING OPTIONS"
msgstr "OPTIONS D'OBSERVATION"

#. type: TP
#: C/man8/debugfs.reiser4.8:45
#, no-wrap
msgid "B<-k, --cat>"
msgstr "B<-k>, B<--cat>"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:48
msgid "browses passed file like standard cat and ls programs."
msgstr ""
"Naviguer dans les fichiers fournis comme avec les programmes standard B<cat> "
"et B<ls>."

#. type: SH
#: C/man8/debugfs.reiser4.8:48
#, no-wrap
msgid "PRINT OPTIONS"
msgstr "OPTIONS D'AFFICHAGE"

#. type: TP
#: C/man8/debugfs.reiser4.8:49
#, no-wrap
msgid "B<-t, --print-tree>"
msgstr "B<-t>, B<--print-tree>"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:52
msgid "prints the internal tree."
msgstr "Afficher l'arbre interne."

#. type: TP
#: C/man8/debugfs.reiser4.8:52
#, no-wrap
msgid "B<-b, --print-block N>"
msgstr "B<-b>, B<--print-block> I<n>"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:55
msgid "prints the block associated with the passed block number."
msgstr "Afficher le bloc associé au I<n>uméro de bloc indiqué."

#. type: TP
#: C/man8/debugfs.reiser4.8:55
#, no-wrap
msgid "B<-n, --print-nodes FILE>"
msgstr "B<-n>, B<--print-nodes> I<fichier>"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:58
msgid "prints all nodes that the passed file lies in."
msgstr "Afficher tous les nœuds sur lesquels réside le I<fichier> indiqué."

#. type: TP
#: C/man8/debugfs.reiser4.8:58
#, no-wrap
msgid "B<-i, --print-file>"
msgstr "B<-i>, B<--print-file>"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:61
msgid "prints all items that the passed file consists of."
msgstr "Afficher tous les éléments qui constituent le fichier fourni."

#. type: TP
#: C/man8/debugfs.reiser4.8:61
#, no-wrap
msgid "B<-s, --print-super>"
msgstr "B<-s>, B<--print-super>"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:64
msgid ""
"prints the both super blocks: master super block and format specific one."
msgstr ""
"Afficher les deux superblocs : le superbloc maître et celui spécifique au "
"format."

#. type: TP
#: C/man8/debugfs.reiser4.8:64
#, no-wrap
msgid "B<-j, --print-journal>"
msgstr "B<-j>, B<--print-journal>"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:67
msgid "prints the journal with not yet commited transactions (if any)."
msgstr ""
"Afficher le journal avec les transactions pas encore enregistrées (s'il y en "
"a)."

#. type: TP
#: C/man8/debugfs.reiser4.8:67
#, no-wrap
msgid "B<-a, --print-alloc>"
msgstr "B<-a>, B<--print-alloc>"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:70
msgid "prints the block allocator data."
msgstr "Afficher les données concernant l'allocation de blocs."

#. type: TP
#: C/man8/debugfs.reiser4.8:70
#, no-wrap
msgid "B<-d, --print-oid>"
msgstr "B<-d>, B<--print-oid>"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:73
msgid "prints the oid allocator data."
msgstr "Afficher les données concernant l'allocation d'oid."

#. type: SH
#: C/man8/debugfs.reiser4.8:73
#, no-wrap
msgid "METADATA OPTIONS"
msgstr "OPTIONS DES MÉTADONNÉES"

#. type: TP
#: C/man8/debugfs.reiser4.8:74
#, no-wrap
msgid "B<-P, --pack-metadata>"
msgstr "B<-P>, B<--pack-metadata>"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:77
msgid "fetches filesystem metadata and writes them to the standard output."
msgstr ""
"Récupérer les métadonnées d'un système de fichiers et les écrire sur la "
"sortie standard."

#. type: TP
#: C/man8/debugfs.reiser4.8:77
#, no-wrap
msgid "B<-U, --unpack-metadata>"
msgstr "B<-U>, B<--unpack-metadata>"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:83
msgid ""
"reads filesystem metadata stream from the stdandard input and constructs a "
"new filesystem based on the metadata. debugfs.reiser4 --pack-metadata "
"E<lt>FS1E<gt> | debugfs.reiser4 --unpack-metadata E<lt>FS2E<gt> and then "
"debugfs.reiser4 --pack-metadata E<lt>FS2E<gt> produces a stream equivalent "
"to the first one."
msgstr ""
"Lire un flux de métadonnées d'un système de fichiers sur l'entrée standard "
"et construire un nouveau système de fichiers basé sur ces métadonnées. "
"B<debugfs.reiser4 --pack-metadata> I<FS1E> B<| debugfs.reiser4 --unpack-"
"metadata> I<FS2E> puis B<debugfs.reiser4 --pack-metadata> I<FS2E> produit un "
"flux équivalent au premier."

#. type: SH
#: C/man8/debugfs.reiser4.8:83 C/man8/mkfs.reiser4.8:59
#: C/man8/fsck.reiser4.8:56 C/man8/measurefs.reiser4.8:80
#, no-wrap
msgid "PLUGIN OPTIONS"
msgstr "OPTIONS POUR LES GREFFONS"

#. type: TP
#: C/man8/debugfs.reiser4.8:84 C/man8/mkfs.reiser4.8:60
#: C/man8/measurefs.reiser4.8:81
#, no-wrap
msgid "B<-p, --print-profile>"
msgstr "B<-p>, B<--print-profile>"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:89 C/man8/mkfs.reiser4.8:65
#: C/man8/fsck.reiser4.8:62 C/man8/measurefs.reiser4.8:86
msgid ""
"prints the plugin profile. This is the set of default plugins used for all "
"parts of a filesystem -- format, nodes, files, directories, hashes, etc. If "
"--override is specified, then prints modified plugins."
msgstr ""
"Afficher le profil des greffons. Il s'agit de l'ensemble des greffons "
"utilisés pour toutes les parties d'un système de fichiers — le format, les "
"nœuds, les fichiers, les répertoires, les tables de hachage, etc. Si B<--"
"override> est utilisée, alors afficher les greffons modifiés."

#. type: TP
#: C/man8/debugfs.reiser4.8:89 C/man8/mkfs.reiser4.8:65
#: C/man8/fsck.reiser4.8:62 C/man8/measurefs.reiser4.8:86
#, no-wrap
msgid "B<-l, --print-plugins>"
msgstr "B<-l>, B<--print-plugins>"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:92
msgid "prints all plugins libreiser4 knows about."
msgstr "Afficher tous les greffons connus de libreiser4."

#. type: TP
#: C/man8/debugfs.reiser4.8:92 C/man8/mkfs.reiser4.8:68
#: C/man8/fsck.reiser4.8:65 C/man8/measurefs.reiser4.8:89
#, no-wrap
msgid "B<-o, --override TYPE=PLUGIN, ...>"
msgstr "B<-o>, B<--override> I<type>B<=>I<greffon>[B<,>I<type>B<=>I<greffon> ...]"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:96 C/man8/mkfs.reiser4.8:72
#: C/man8/fsck.reiser4.8:69 C/man8/measurefs.reiser4.8:93
msgid ""
"overrides the default plugin of the type \"TYPE\" by the plugin \"PLUGIN\" "
"in the plugin profile."
msgstr ""
"Remplacer le greffon par défaut pour le I<type> par le I<greffon> dans le "
"profil des greffons."

#. type: Plain text
#: C/man8/debugfs.reiser4.8:96 C/man8/mkfs.reiser4.8:74
#: C/man8/measurefs.reiser4.8:73 C/man8/measurefs.reiser4.8:95
#, no-wrap
msgid "Examples:"
msgstr "Exemples :"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:99
msgid "debugfs.reiser4 -o nodeptr=nodeptr41,hash=rupasov_hash /dev/hda2"
msgstr "debugfs.reiser4 -o nodeptr=nodeptr41,hash=rupasov_hash /dev/hda2"

#. type: SH
#: C/man8/debugfs.reiser4.8:100 C/man8/mkfs.reiser4.8:80
#: C/man8/fsck.reiser4.8:92 C/man8/measurefs.reiser4.8:98
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORT DE BOGUES"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:102 C/man8/mkfs.reiser4.8:82
#: C/man8/fsck.reiser4.8:94 C/man8/measurefs.reiser4.8:100
msgid "Report bugs to E<lt>reiserfs-devel@vger.kernel.orgE<gt>"
msgstr "Signaler les bogues à E<lt>I<reiserfs-devel@vger.kernel.org>E<gt>"

#. type: SH
#: C/man8/debugfs.reiser4.8:102 C/man8/mkfs.reiser4.8:82
#: C/man8/fsck.reiser4.8:94 C/man8/measurefs.reiser4.8:100
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:106
msgid "B<measurefs.reiser4(8),> B<mkfs.reiser4(8),> B<fsck.reiser4(8)>"
msgstr "B<fsck.reiser4>(8), B<measurefs.reiser4>(8), B<mkfs.reiser4>(8)"

#. type: SH
#: C/man8/debugfs.reiser4.8:106 C/man8/mkfs.reiser4.8:86
#: C/man8/fsck.reiser4.8:98 C/man8/measurefs.reiser4.8:104
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#. type: Plain text
#: C/man8/debugfs.reiser4.8:107 C/man8/mkfs.reiser4.8:88
#: C/man8/measurefs.reiser4.8:105
msgid "This manual page was written by Yury Umanets E<lt>umka@namesys.comE<gt>"
msgstr ""
"Cette page de manuel a été écrite par Yury Umanets E<lt>I<umka@namesys."
"com>E<gt>"

#. type: TH
#: C/man8/mkfs.reiser4.8:5
#, no-wrap
msgid "mkfs.reiser4"
msgstr "mkfs.reiser4"

#. type: Plain text
#: C/man8/mkfs.reiser4.8:20
msgid "mkfs.reiser4 - the program for creating reiser4 filesystem."
msgstr "mkfs.reiser4 - Créer un système de fichiers reiser4"

#. type: Plain text
#: C/man8/mkfs.reiser4.8:23
msgid "B<mkfs.reiser4> [ options ] FILE1 FILE2 ... [ size[K|M|G] ]"
msgstr ""
"B<mkfs.reiser4> [I<options>] I<fichier1 fichier2> ... [I<taille>[B<K>|B<M>|"
"B<G>]]"

#. type: Plain text
#: C/man8/mkfs.reiser4.8:29
msgid ""
"B<mkfs.reiser4> is reiser4 filesystem creation program. It is based on new "
"libreiser4 library. Since libreiser4 is fully plugin-based, we have the "
"potential to create not just reiser4 partitions, but any filesystem or "
"database format, which is based on balanced trees."
msgstr ""
"B<mkfs.reiser4> est un programme pour créer des systèmes de fichiers "
"Reiser4. Il est basé sur la nouvelle bibliothèque libreiser4. Comme "
"libreiser4 repose sur des greffons, il est possible de créer non pas "
"uniquement des partitions Reiser4, mais tout type de système de fichiers ou "
"de base de données qui utilise des arbres « balanced trees »."

#. type: Plain text
#: C/man8/mkfs.reiser4.8:42
msgid "forces mkfs to use whole disk, not block device or mounted partition."
msgstr ""
"Forcer mkfs à utiliser tout le disque, au lieu d'un périphérique bloc ou "
"d'une partition montée."

#. type: SH
#: C/man8/mkfs.reiser4.8:42
#, no-wrap
msgid "MKFS OPTIONS"
msgstr "OPTIONS DE MKFS"

#. type: TP
#: C/man8/mkfs.reiser4.8:43
#, no-wrap
msgid "B<-b, --block-size N>"
msgstr "B<-b>, B<--block-size> I<n>"

#. type: Plain text
#: C/man8/mkfs.reiser4.8:46
msgid "block size to be used (architecture page size by default)"
msgstr ""
"Taille de bloc à utiliser (par défaut, la taille de page de l'architecture)."

#. type: TP
#: C/man8/mkfs.reiser4.8:46
#, no-wrap
msgid "B<-L, --label LABEL>"
msgstr "B<-L>, B<--label> I<étiquette>"

#. type: Plain text
#: C/man8/mkfs.reiser4.8:49
msgid "volume label to be used"
msgstr "Étiquette de volume à utiliser."

#. type: TP
#: C/man8/mkfs.reiser4.8:49
#, no-wrap
msgid "B<-U, --uuid UUID>"
msgstr "B<-U>, B<--uuid> I<UUID>"

#. type: Plain text
#: C/man8/mkfs.reiser4.8:52
msgid "universally unique identifier to be used"
msgstr "Identifiant unique universel à utiliser."

#. type: TP
#: C/man8/mkfs.reiser4.8:52
#, no-wrap
msgid "B<-s, --lost-found>"
msgstr "B<-s>, B<--lost-found>"

#. type: Plain text
#: C/man8/mkfs.reiser4.8:55
msgid "forces mkfs to create lost+found directory."
msgstr "Forcer mkfs à créer le répertoire I<lost+found>."

#. type: TP
#: C/man8/mkfs.reiser4.8:55
#, no-wrap
msgid "B<-d, --discard>"
msgstr "B<-d>, B<--discard>"

#. type: Plain text
#: C/man8/mkfs.reiser4.8:59
msgid ""
"tells mkfs to discard given device before creating the filesystem (for solid "
"state drives)."
msgstr ""
"Indiquer à B<mkfs> d’abandonner les blocs du périphérique donné avant de "
"créer le système de ficher (pour les SSD, « solid-state drive »)."

#. type: Plain text
#: C/man8/mkfs.reiser4.8:68 C/man8/fsck.reiser4.8:65
#: C/man8/measurefs.reiser4.8:89
msgid "prints all plugins libreiser4 know about."
msgstr "Afficher tous les greffons connus de libreiser4."

#. type: Plain text
#: C/man8/mkfs.reiser4.8:77
msgid ""
"assign short key plugin to \"key\" field in order to create filesystem with "
"short keys policy:"
msgstr ""
"Mettre dans le champ « key » le greffon pour des clefs courtes de manière à "
"créer un système de fichiers avec une politique de clefs courtes :"

#. type: Plain text
#: C/man8/mkfs.reiser4.8:79
msgid "mkfs.reiser4 -yf -o key=key_short /dev/hda2"
msgstr "mkfs.reiser4 -yf -o key=key_short /dev/hda2"

#. type: Plain text
#: C/man8/mkfs.reiser4.8:86
msgid "B<measurefs.reiser4(8),> B<debugfs.reiser4(8),> B<fsck.reiser4(8)>"
msgstr "B<measurefs.reiser4>(8), B<debugfs.reiser4>(8), B<fsck.reiser4>(8)"

#. type: TH
#: C/man8/fsck.reiser4.8:5
#, no-wrap
msgid "fsck.reiser4"
msgstr "fsck.reiser4"

#. type: TH
#: C/man8/fsck.reiser4.8:5
#, no-wrap
msgid "05 February, 2004"
msgstr "5 février 2004"

#. type: Plain text
#: C/man8/fsck.reiser4.8:20
msgid ""
"fsck.reiser4 - the program for checking and repairing reiser4 filesystem."
msgstr "fsck.reiser4 - Vérifier et réparer un système de fichiers Reiser4"

#. type: Plain text
#: C/man8/fsck.reiser4.8:23
msgid "B<fsck.reiser4> [ options ] FILE"
msgstr "B<fsck.reiser4> [I<options>] I<fichier>"

#. type: Plain text
#: C/man8/fsck.reiser4.8:26
msgid "B<fsck.reiser4> is reiser4 filesystem check and repair program."
msgstr ""
"B<fsck.reiser4> est le programme de vérification et de réparation des "
"systèmes de fichiers Reiser4."

#. type: SH
#: C/man8/fsck.reiser4.8:26
#, no-wrap
msgid "CHECK OPTIONS"
msgstr "OPTIONS DE VÉRIFICATION"

#. type: TP
#: C/man8/fsck.reiser4.8:27
#, no-wrap
msgid "B<--check>"
msgstr "B<--check>"

#. type: Plain text
#: C/man8/fsck.reiser4.8:31
msgid ""
"the default action checks the consistency and reports, but does not repair "
"any corruption that it finds.  This option may be used on a read-only file "
"system mount."
msgstr ""
"Cette action par défaut vérifie la cohérence et fournit un rapport, mais ne "
"corrige pas les corruptions trouvées. Cette option peut être utilisée sur un "
"système de fichiers monté en lecture seule."

#. type: TP
#: C/man8/fsck.reiser4.8:31
#, no-wrap
msgid "B<--fix>"
msgstr "B<--fix>"

#. type: Plain text
#: C/man8/fsck.reiser4.8:35
msgid ""
"fixes minor corruptions that do not require rebuilding; sets up correct "
"values of bytes unsupported by kernel in the case of transparent compression."
msgstr ""
"Corriger les corruptions mineures qui ne nécessitent pas une "
"reconstruction ; mettre des valeurs correctes pour les octets qui ne sont "
"pas pris en charge par le noyau dans le cas d'une compression transparente."

#. type: TP
#: C/man8/fsck.reiser4.8:35
#, no-wrap
msgid "B<--build-sb>"
msgstr "B<--build-sb>"

#. type: Plain text
#: C/man8/fsck.reiser4.8:38
msgid ""
"fixes all severe corruptions in super blocks, rebuilds super blocks from "
"scratch if needed."
msgstr ""
"Corriger toutes les corruptions majeures des superblocs, reconstruire "
"complètement les superblocs si nécessaire."

#. type: TP
#: C/man8/fsck.reiser4.8:38
#, no-wrap
msgid "B<--build-fs>"
msgstr "B<--build-fs>"

#. type: Plain text
#: C/man8/fsck.reiser4.8:41
msgid ""
"fixes all severe fs corruptions, except super block ones; rebuilds reiser4 "
"filesystem from the scratch if needed."
msgstr ""
"Corriger toutes les corruptions majeures du système de fichiers, à "
"l'exception des corruptions de superblocs ; reconstruire complètement un "
"système de fichiers Reiser4 si nécessaire."

#. type: TP
#: C/man8/fsck.reiser4.8:41
#, no-wrap
msgid "B<-L, --logfile>"
msgstr "B<-L>, B<--logfile>"

#. type: Plain text
#: C/man8/fsck.reiser4.8:44
msgid ""
"forces fsck to report any corruption it finds to the specified logfile "
"rather then on stderr."
msgstr ""
"Forcer B<fsck> à fournir le rapport des corruptions dans le fichier indiqué "
"au lieu de la sortie d'erreur standard."

#. type: TP
#: C/man8/fsck.reiser4.8:44
#, no-wrap
msgid "B<-n, --no-log>"
msgstr "B<-n>, B<--no-log>"

#. type: Plain text
#: C/man8/fsck.reiser4.8:47
msgid "prevents fsck from reporting any kind of corruption."
msgstr "Désactiver l'affichage de tout type de corruption trouvé par B<fsck>."

#. type: TP
#: C/man8/fsck.reiser4.8:47
#, no-wrap
msgid "B<-a, --auto>"
msgstr "B<-a>, B<--auto>"

#. type: Plain text
#: C/man8/fsck.reiser4.8:50
msgid "automatically checks the file system without any questions."
msgstr "Vérifier automatiquement le système de fichiers sans aucune question."

#. type: TP
#: C/man8/fsck.reiser4.8:50 C/man8/fsck.reiser4.8:76
#, no-wrap
msgid "B<-q, --quiet>"
msgstr "B<-q>, B<--quiet>"

#. type: Plain text
#: C/man8/fsck.reiser4.8:53
msgid "supresses gauges."
msgstr "Supprimer les barres de progression."

#. type: TP
#: C/man8/fsck.reiser4.8:53
#, no-wrap
msgid "B<-r>"
msgstr "B<-r>"

#. type: Plain text
#: C/man8/fsck.reiser4.8:56
msgid "ignored."
msgstr "Ignorée."

#. type: TP
#: C/man8/fsck.reiser4.8:57
#, no-wrap
msgid "B<--print-profile>"
msgstr "B<--print-profile>"

#. type: Plain text
#: C/man8/fsck.reiser4.8:73
msgid "prints program version"
msgstr "Afficher la version du programme."

#. type: Plain text
#: C/man8/fsck.reiser4.8:76
msgid "prints program help"
msgstr "Afficher l'aide du programme."

#. type: Plain text
#: C/man8/fsck.reiser4.8:79
msgid "suppress messages."
msgstr "Supprimer les messages."

#. type: Plain text
#: C/man8/fsck.reiser4.8:85
msgid "forces fsck to use whole disk, not block device or mounted partition."
msgstr ""
"Forcer B<fsck> à utiliser tout le disque, au lieu d'un périphérique bloc ou "
"une partition montée."

#. type: TP
#: C/man8/fsck.reiser4.8:85
#, no-wrap
msgid "B<-p, --preen>"
msgstr "B<-p>, B<--preen>"

#. type: Plain text
#: C/man8/fsck.reiser4.8:88
msgid "automatically repair minor corruptions on the filesystem."
msgstr ""
"Réparer automatiquement les corruptions mineures du système de fichiers."

#. type: Plain text
#: C/man8/fsck.reiser4.8:91
msgid "tunes number of nodes in the libreiser4 tree buffer cache"
msgstr ""
"Configurer le nombre de nœuds dans le cache d'arbres tampons de libreiser4."

#. type: Plain text
#: C/man8/fsck.reiser4.8:98
msgid "B<debugfs.reiser4(8),> B<mkfs.reiser4(8),> B<measurefs.reiser4(8)>"
msgstr "B<debugfs.reiser4>(8), B<mkfs.reiser4>(8), B<measurefs.reiser4>(8)"

#. type: Plain text
#: C/man8/fsck.reiser4.8:99
msgid ""
"This manual page was written by Vitaly Fertman E<lt>vitaly@namesys.comE<gt>"
msgstr ""
"Cette page de manuel a été écrite par Vitaly Fertman E<lt>I<vitaly@namesys."
"com>E<gt>"

#. type: TH
#: C/man8/measurefs.reiser4.8:5
#, no-wrap
msgid "measurefs.reiser4"
msgstr "measurefs.reiser4"

#. type: TH
#: C/man8/measurefs.reiser4.8:5
#, no-wrap
msgid "28 Apr, 2003"
msgstr "28 avril 2003"

#. type: Plain text
#: C/man8/measurefs.reiser4.8:21
msgid ""
"measurefs.reiser4 - the program for measuring reiser4 filesystem parameters "
"(fragmentation, node packing, etc.)."
msgstr ""
"measurefs.reiser4 - Le programme de mesure des paramètres d'un système de "
"fichiers Reiser4 (fragmentation, empilement des nœuds, etc.)"

#. type: Plain text
#: C/man8/measurefs.reiser4.8:24
msgid "B<measurefs.reiser4> [ options ] FILE"
msgstr "B<measurefs.reiser4> [I<options>] I<fichier>"

#. type: Plain text
#: C/man8/measurefs.reiser4.8:28
msgid ""
"B<measurefs.reiser4> is reiser4 filesystem measure program. You can estimate "
"reiser4 filesystem fragmentation, packingm etc. structures by using it."
msgstr ""
"B<measurefs.reiser4> est un programme qui permet de mesurer un système de "
"fichiers Reiser4. En l'utilisant, vous pouvez estimer la fragmentation, "
"l'empilement des structures, etc. d'un système de fichiers."

#. type: Plain text
#: C/man8/measurefs.reiser4.8:41
msgid ""
"forces measurefs to use whole disk, not block device or mounted partition."
msgstr ""
"Forcer B<measurefs> à utiliser un disque entier, au lieu d'un périphérique "
"bloc ou une partition montée."

#. type: SH
#: C/man8/measurefs.reiser4.8:45
#, no-wrap
msgid "MEASUREMENT OPTIONS"
msgstr "OPTIONS DE MESURE"

#. type: TP
#: C/man8/measurefs.reiser4.8:46
#, no-wrap
msgid "B<-S, --tree-stat>"
msgstr "B<-S>, B<--tree-stat>"

#. type: Plain text
#: C/man8/measurefs.reiser4.8:49
msgid ""
"shows different tree statistics (node packing, internal nodes, leaves, etc)"
msgstr ""
"Afficher différentes statistiques des arbres (empilement des nœuds, nœuds "
"internes, feuilles, etc.)"

#. type: TP
#: C/man8/measurefs.reiser4.8:49
#, no-wrap
msgid "B<-T, --tree-frag>"
msgstr "B<-T>, B<--tree-frag>"

#. type: Plain text
#: C/man8/measurefs.reiser4.8:54
msgid ""
"measures total tree fragmentation. The result is fragmentation factor - "
"value from 0.00000 (minimal fragmentation) to 1.00000 (maximal one). Most "
"probably, this factor may affect sequential read performance."
msgstr ""
"Mesurer la fragmentation totale de l'arbre. Le résultat est un coefficient "
"de fragmentation, une valeur comprise entre 0,000 00 (la fragmentation "
"minimale) et 1,000 00 (la fragmentation maximale). Ce coefficient affectera "
"très probablement les lectures séquentielles."

#. type: TP
#: C/man8/measurefs.reiser4.8:54
#, no-wrap
msgid "B<-D, --data-frag>"
msgstr "B<-D>, B<--data-frag>"

#. type: Plain text
#: C/man8/measurefs.reiser4.8:60
msgid ""
"measures average files fragmentation. This means, that fragmentation of each "
"file in filesystem will be measured separately and results will be averaged. "
"The result is fragmentation factor - value from 0.00000 (minimal "
"fragmentation) to 1.00000 (maximal one)."
msgstr ""
"Mesurer la fragmentation moyenne des fichiers. C'est-à-dire que la "
"fragmentation de chaque fichier sera mesurée séparément pour ensuite obtenir "
"une moyenne. Le résultat est un coefficient de fragmentation, une valeur "
"comprise entre 0,000 00 (la fragmentation minimale) et 1,000 00 (la "
"fragmentation maximale)."

#. type: Plain text
#: C/man8/measurefs.reiser4.8:63
msgid ""
"Note, that for the fresh filesystem (created not very long time ago)  and "
"even fully filled by data, this value will be pretty small."
msgstr ""
"Remarquez que pour un système de fichier récent, et même s'il est rempli de "
"données, cette valeur sera très faible."

#. type: TP
#: C/man8/measurefs.reiser4.8:63
#, no-wrap
msgid "B<-F, --file-frag FILE>"
msgstr "B<-F>, B<--file-frag> I<fichier>"

#. type: Plain text
#: C/man8/measurefs.reiser4.8:71
msgid ""
"measures fragmentation of the specified file. The result is fragmentation "
"factor - value from 0.00000 (minimal fragmentation) to 1.00000 (maximal "
"one). Note, that fragmentation of a small file (depends of used tail "
"policy), which consists of tail items, is not very reliable value. That is "
"because, they is always afoot due to balancing."
msgstr ""
"Mesurer la fragmentation du I<fichier> indiqué. Le résultat est un quotient "
"de fragmentation allant de la valeur 0,000 00 (fragmentation minimale) à "
"1,000 00 (la fragmentation maximale). Notez que la fragmentation d'un petit "
"fichier, qui consiste en des éléments de queue (en fonction de la politique "
"des queues), n'est pas une valeur très fiable. C’est parce qu'ils sont "
"toujours déplacés pour l'équilibrage."

#. type: Plain text
#: C/man8/measurefs.reiser4.8:75
msgid "measurefs.reiser4 -F /usr/bin /dev/hda2"
msgstr "measurefs.reiser4 -F /usr/bin /dev/hda2"

#. type: Plain text
#: C/man8/measurefs.reiser4.8:77
msgid "measurefs.reiser4 -F /bin/bash /dev/hda2"
msgstr "measurefs.reiser4 -F /bin/bash /dev/hda2"

#. type: TP
#: C/man8/measurefs.reiser4.8:77
#, no-wrap
msgid "B<-E, --show-file>"
msgstr "B<-E>, B<--show-file>"

#. type: Plain text
#: C/man8/measurefs.reiser4.8:80
msgid "show file fragmentation for each file if --data-frag is specified."
msgstr ""
"Afficher la fragmentation de chaque fichier si B<--data-frag> est utilisée."

#. type: Plain text
#: C/man8/measurefs.reiser4.8:97
msgid "measurefs.reiser4 -o nodeptr=nodeptr41,hash=rupasov_hash /dev/hda2"
msgstr "measurefs.reiser4 -o nodeptr=nodeptr41,hash=rupasov_hash /dev/hda2"

#. type: Plain text
#: C/man8/measurefs.reiser4.8:104
msgid "B<debugfs.reiser4(8),> B<mkfs.reiser4(8),> B<fsck.reiser4(8)>"
msgstr "B<debugfs.reiser4>(8), B<mkfs.reiser4>(8), B<fsck.reiser4>(8)"
